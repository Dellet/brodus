<?php
if (isset($_POST['email'])) {

    // EDIT THE 2 LINES BELOW AS REQUIRED
    $email_from_to = "malakhov08@gmail.com";
    $email_from_subject = "Topic";

    function died($error)
    {
        // your error code can go here
        echo "We are very sorry, but there were error(s) found with the form you submitted." . "\r\n";
        echo "These errors appear below." . "\r\n";
        echo $error . "\n\n";
        echo "Please go back and fix these errors." . "\r\n";
        die();
    }


    // validation expected data exists
    if (
        !isset($_POST['name']) ||
        !isset($_POST['email']) ||
        !isset($_POST['message'])
    ) {
        died('We are sorry, but there appears to be a problem with the form you submitted.' . "\r\n");
    }

    $name = $_POST['name']; // required
    $email_from = $_POST['email']; // required
    $message = $_POST['message']; // required

    $error_message = "";
    $email_from_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';

    if (!preg_match($email_from_exp, $email_from)) {
        $error_message .= "\r\n" . 'The Email Address you entered does not appear to be valid.' . "\r\n";
    }

    $string_exp = "/^[A-Za-z .'-]+$/";

    if (!preg_match($string_exp, $name)) {
        $error_message .= "\r\n" . 'The First Name you entered does not appear to be valid.' . "\r\n";
    }

    if (strlen($message) < 2) {
        $error_message .= "\r\n" . 'The Comments you entered do not appear to be valid.' . "\r\n";
    }

    if (strlen($error_message) > 0) {
        died($error_message);
    }

    $email_message = "Form details below.\n\n";


    function clean_string($string)
    {
        $bad = array("content-type", "bcc:", "to:", "cc:", "href");
        return str_replace($bad, "", $string);
    }



    $email_message .= "Name: " . clean_string($name) . "\n";
    $email_message .= "Email: " . clean_string($email_from) . "\n";
    $email_message .= "Comments: " . clean_string($message) . "\n";

    // create email headers
    $headers = 'From: ' . $email_from . "\r\n" .
        'Reply-To: ' . $email_from_from . "\r\n" .
        'X-Mailer: PHP/' . phpversion();
    @mail($email_from_to, $email_from_subject, $email_message, $headers);
    ?>

    Thank you for contacting us. We will be in touch with you very soon.

<?php
}
?>